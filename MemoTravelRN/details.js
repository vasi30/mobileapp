import React, {Component} from 'react';
import {
   TouchableHighlight, 
   Image, 
   AppRegistry,
   StyleSheet, 
   Text, 
   View
} from 'react-native';

class DetailScreen extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
         <Text> Name: {this.props.name}</Text>
	 <Text> Rating: {this.props.rating} * </Text>
	 <Text> Coordinates: {this.props.coordinates} </Text>
   <Text> Review: {this.props.review} </Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container:{
    flex:1,
    padding: 10,
    paddingTop:70,
  },
});

export default DetailScreen;
