import React from 'react';
import {
  View,
  ListView,
  StyleSheet,
  Navigator,
  TouchableOpacity,
  Text,
  TouchableHighlight
} from 'react-native';

// import * as Progress from 'react-native-progress';

import InfiniteScrollView from 'react-native-infinite-scroll-view';

var MOCKED_DESTINATIONS_DATA = [
  { name: 'Tahiti', rating: '5', coordinates: "1,2", review: "It`s a magical place" },
  { name: 'Tahiti', rating: '5', coordinates: "1,2", review: "It`s a magical place" },
  { name: 'Tahiti', rating: '5', coordinates: "1,2", review: "It`s a magical place" },
];

var DomParser = require('xmldom').DOMParser;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 60
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: 'blue',
    marginBottom: 5,
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
  progress: {
    marginTop: 80,
  },
});

class ListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      destinationsSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2
      }),
      loaded: true
    };

    this.items = [
      { name: "Tahiti", coordinates: "123, 12", rating: "5", review: "It`s a magical place" },
      { name: "Cluj-Napoca", coordinates: "115, 22", rating: "1", review: "It`s a magical place" },
      { name: "Numbani", coordinates: "12, 112", rating: "3", review: "It`s a magical place" },

    ];
  }

  componentDidMount() {
    this.setState({
      destinationsSource: this.state.destinationsSource.cloneWithRows(this.items)
    });
  }
  render() {
    if (!this.state.loaded) {
      return (<View style={styles.progress}>
        <Text>Please wait ... </Text>
        {/*<Progress.Bar progress={0.3} width={200} indeterminate={true} />*/}
      </View>
      );
    }
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.destinationsSource}
          renderRow={(rowData) =>
            <TouchableOpacity onPress={() => this.props.navigator.push({
              index: 1,
              passProps: {
                name: rowData.name,
                coordinates: rowData.coordinates,
                rating: rowData.rating,
                review: rowData.review
              }
            })}>
              <Text style={styles.instructions}>{rowData.name}({rowData.coordinates})</Text>
            </TouchableOpacity>
          } />
      </View>
    );
  }

  renderDestination(rowData) {
    return (
      <TouchableOpacity onPress={() => this.props.navigator.push({
        index: 1,
        passProps: {
          name: rowData.name,
          coordinates: rowData.coordinates,
          rating: rowData.rating,
          review: rowData.review
        }
      })}>
        <Text style={styles.instructions}>{rowData.name}({rowData.coordinates})</Text>
      </TouchableOpacity>
    )
  }
}

export default ListScreen;

