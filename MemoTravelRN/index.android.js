/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import index from './index.js';
// import React, { Component } from 'react';
// import {
//     AppRegistry,
//     StyleSheet,
//     Text,
//     View,
//     ListView,
//     TextInput,
//     TouchableHighlight
// } from 'react-native';

// class MemoTravelRN extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             destinationsSource: new ListView.DataSource({
//                 rowHasChanged: (row1, row2) => row1 !== row2
//             })
//         };

//         this.items = [
//             { name: "Tahiti", coordinates: "123, 12", rating: "5", review: "It`s a magical place" },
//             { name: "Tahiti", coordinates: "123, 12", rating: "5", review: "It`s a magical place" },
//             { name: "Tahiti", coordinates: "123, 12", rating: "5", review: "It`s a magical place" },

//         ];
//     }

//     componentDidMount() {
//         this.setState({
//             destinationsSource: this.state.destinationsSource.cloneWithRows(this.items)
//         });
//     }

//     render() {
//         return (
//             <View style={styles.container}>
//                 <ListView
//                     dataSource={this.state.destinationsSource}
//                     renderRow={this.renderDestination} />
//             </View>
//         )
//     }

//     renderDestination(rowData) {
//         return (
//             <TouchableHighlight onPress={() => navigator.pop()}>
//             <Text style={styles.instructions}>{rowData.name}({rowData.coordinates})</Text>
//         </TouchableHighlight>
//         )
//     }
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: 'darkblue',
//     },
//     welcome: {
//         fontSize: 20,
//         textAlign: 'center',
//         margin: 10,
//     },
//     instructions: {
//         textAlign: 'center',
//         color: 'white',
//         marginBottom: 5,
//     },
// });

// AppRegistry.registerComponent('MemoTravelRN', () => MemoTravelRN);
