import React, { Component } from 'react';
import {
    TouchableHighlight,
    Image,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
    ToastAndroid
} from 'react-native';

import Communications from 'react-native-communications';

class NewDestination extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            coordinates: "",
            rating: '0',
            review: "",
            email: ""
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <Text> Name: </Text>
                <TextInput style={styles.textInput}
                    onChangeText={(name) => this.setState({ name })}
                    value={this.state.name}
                    />
                <Text> Rating: </Text>
                <TextInput style={styles.textInput}
                    onChangeText={(coordinates) => this.setState({ coordinates })}
                    value={this.state.coordinates}
                    />
                <Text> Coordinates: </Text>
                <TextInput style={styles.textInput}
                    onChangeText={(rating) => this.setState({ rating })}
                    value={this.state.rating}
                    />
                <Text> Review: </Text>
                <TextInput style={styles.textInput}
                    onChangeText={(review) => this.setState({ review })}
                    value={this.state.review}
                    />
                <Text> Email: </Text>
                <TextInput style={styles.textInput}
                    onChangeText={(email) => this.setState({ email })}
                    value={this.state.email}
                    />
                <TouchableHighlight onPress={() => this.sendData()}>
                    <Text style={styles.navigationBarText}>sendData</Text>
                </TouchableHighlight>
            </View>
        );
    }

    sendData() {
        var body = "Please check this new destination: \n" +
        "name: " + this.state.name + "\n" +
        "coordinates: " + this.state.coordinates + "\n" +
        "rating: " + this.state.rating + "\n" +
        "review: " + this.state.review + "\n";

        Communications.text('0123456789', body)

        //Communications.email(['vasi30@gmail.com', 'vasi30@yahoo.com'],null,null,this.state.name,body)
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingTop: 70,
    },
    textInput: {
        borderColor: 'darkred',
        borderWidth: 0
    }
});

export default NewDestination;
