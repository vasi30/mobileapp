
<script src="https://www.gstatic.com/firebasejs/3.6.5/firebase.js"></script>

// Initialize Firebase
var config = {
    apiKey: "AIzaSyC2YLXbnmyo9OyC05bgDrLCXC4OnXcEQjM",
    authDomain: "memotravel-4d997.firebaseapp.com",
    databaseURL: "https://memotravel-4d997.firebaseio.com",
    storageBucket: "memotravel-4d997.appspot.com",
    messagingSenderId: "970206132432"
};
Firebase.initializeApp(config);

var firebase = require("firebase/app");
require("firebase/auth");
require("firebase/database");

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ListView,
    ScrollView,
    Image,
    TextInput,
    Navigator,
    TouchableOpacity,
    Alert,
    TouchableHighlight,
    AsyncStorage,
} from 'react-native';

import Firebase from 'firebase';
import Communications from 'react-native-communications';
import { BarChart } from 'react-native-charts'
import Button from 'react-native-button';

var destinations = []

var SCREEN_WIDTH = require('Dimensions').get('window').width;

var BaseConfig = Navigator.SceneConfigs.FloatFromRight;

var CustomLeftToRightGesture = Object.assign({}, BaseConfig.gestures.pop, {
    // Make it snap back really quickly after canceling pop
    snapVelocity: 8,
    // Make it so we can drag anywhere on the screen
    edgeHitWidth: SCREEN_WIDTH,
});

var CustomSceneConfig = Object.assign({}, BaseConfig, {
    // A very tighly wound spring will make this transition fast
    springTension: 100,
    springFriction: 1,
    // Use our custom gesture defined above
    gestures: {
        pop: CustomLeftToRightGesture,
    }
});


class MyScene extends React.Component {
    constructor(props) {
        super(props);
        this.itemsRef = Firebase.database().ref();
        this.state = {
            role : props.role
        };
    }

    _handlePress() {
        console.log("testing role" + this.state.role)
        destinations.push({ "name": this.state.name, "rating": this.state.rating, "review" : this.state.review, "coordinates" : this.state.coordinates,
            "rating1" : this.state.rating1, "rating2" : this.state.rating2, "rating3" : this.state.rating3, "rating4" : this.state.rating4, "rating5" : this.state.rating5});

        Communications.email(["vasi30@gmail.com"],"","","ReactEmail","Email sent with information");

        this.itemsRef.push({ "name": this.state.name, "rating": this.state.rating, "review" : this.state.review, "coordinates" : this.state.coordinates,
            "rating1" : this.state.rating1, "rating2" : this.state.rating2, "rating3" : this.state.rating3, "rating4" : this.state.rating4, "rating5" : this.state.rating5});
        // this._persistData();

        this.props.navigator.push({id :4,
        passProps : {
            role : this.state.role
        }});
    }


    // _persistData(){
    //     return AsyncStorage.setItem('destinationsList', JSON.stringify(destinations))
    //         .then(json => console.log('success! at persist save'))
    //         .catch(error => console.log('error! at persist save'));
    // }

    render() {
        return (
            <View style={{padding: 10}}>
              <TextInput
                  style={{height: 60}}
                  placeholder="Destination name"
                  ref= "name"
                  onChangeText={(name) => this.setState({name})}
                  value={this.state.name}
              />
              <TextInput
                  style={{height: 60}}
                  placeholder="Destination rating"
                  ref= "rating"
                  onChangeText={(rating) => this.setState({rating})}
                  value={this.state.rating}
              />
              <TextInput
                  style={{height: 60}}
                  placeholder="Destination review"
                  ref= "review"
                  onChangeText={(review) => this.setState({review})}
                  value={this.state.review}
              />
              <TextInput
                  style={{height: 60}}
                  placeholder="Destination coordinates"
                  ref= "coordinates"
                  onChangeText={(coordinates) => this.setState({coordinates})}
                  value={this.state.coordinates}
              />
              <TextInput
                  style={{height: 60}}
                  placeholder="Rating1"
                  ref= "rating1"
                  onChangeText={(rating1) => this.setState({rating1})}
                  value={this.state.rating1}
              />
              <TextInput
                  style={{height: 60}}
                  placeholder="Rating2"
                  ref= "rating2"
                  onChangeText={(rating2) => this.setState({rating2})}
                  value={this.state.rating2}
              />
              <TextInput
                  style={{height: 60}}
                  placeholder="Rating3"
                  ref= "rating3"
                  onChangeText={(rating3) => this.setState({rating3})}
                  value={this.state.rating3}
              />
            <TextInput
                style={{height: 60}}
                placeholder="Rating4"
                ref= "rating4"
                onChangeText={(rating4) => this.setState({rating4})}
                value={this.state.rating4}
            />
                <TextInput
                    style={{height: 60}}
                    placeholder="Rating5"
                    ref= "rating5"
                    onChangeText={(rating5) => this.setState({rating5})}
                    value={this.state.rating5}
                />

              <Button
                  containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'white'}}
                  style={{fontSize: 18, color: 'blue'}}
                  styleDisabled={{color: 'gray'}}
                  onPress={() => this._handlePress()}>
                Add destination
              </Button>

            </View>
        )
    }
}

class EditScene extends React.Component {

    constructor(props) {
        super(props);

        this.itemsRef = Firebase.database().ref();

        this.state = {
            name : props.destination.name,
            rating : props.destination.rating,
            review : props.destination.review,
            coordinates : props.destination.coordinates,
            rating1 : props.destination.rating1,
            rating2 : props.destination.rating2,
            rating3 : props.destination.rating3,
            rating4 : props.destination.rating4,
            rating5 : props.destination.rating5,
            role : props.role
        };
    }

    // _persistData(){
    //     return AsyncStorage.setItem('destinationsList', JSON.stringify(destinations))
    //         .then(json => console.log('success! save'))
    //         .catch(error => console.log('error! save'));
    // }

    // _handlePress() {
    //     this.props.destination.name = this.state.name;
    //
    //     if(parseInt(this.state.rating) == 1) {
    //         this.props.destination.rating1 = parseInt(this.state.rating1) + 1;
    //     }
    //     if(parseInt(this.state.rating) == 2) {
    //         this.props.destination.rating2 = parseInt(this.state.rating2) + 1;
    //     }
    //     if(parseInt(this.state.rating) == 3) {
    //         this.props.destination.rating3 = parseInt(this.state.rating3) + 1;
    //     }
    //     if(parseInt(this.state.rating) == 4) {
    //         this.props.destination.rating4 = parseInt(this.state.rating4) + 1;
    //     }
    //     if(parseInt(this.state.rating) == 5) {
    //         this.props.destination.rating5 = parseInt(this.state.rating5) + 1;
    //     }
    //
    //
    //     this.props.destination.rating = ((parseFloat(this.state.rating1)*1 +
    //     parseFloat(this.state.rating2)*2 +
    //     parseFloat(this.state.rating3)*3 +
    //     parseFloat(this.state.rating4)*4 +
    //     parseFloat(this.state.rating5)*5)/
    //         (parseFloat(this.state.rating1) +
    //         parseFloat(this.state.rating2) +
    //         parseFloat(this.state.rating3) +
    //         parseFloat(this.state.rating4) +
    //         parseFloat(this.state.rating5))).toString();
    //
    //     this.props.destination.review = this.state.review;
    //     this.props.destination.coordinates = this.state.coordinates;
    //
    //     this._persistData();
    //     this.props.navigator.push({id :1,
    //     passProps : {
    //         role : this.state.role
    //     }});
    // }

    _handlePress(){
        var a = this.state.review;
        var b = this.state.coordinates;
        var name = this.state.name;
        var query = this.itemsRef.orderByChild('name').equalTo(this.props.destination.name);
        query.on('child_added', function(snapshot) {
            snapshot.ref.update({"review" : a, "coordinates" : b, "name":name});
        })

        this.props.navigator.push({id :4,
            passProps : {
                role : this.state.role
            }
        });
    }

    // _handlePressDelete(){
    //     console.log("------------------------ DELETE start  -------------");
    //
    //
    //     var index = destinations.indexOf(this.props.destination);
    //     console.log("index = " + index);
    //
    //     console.log("current length of array: " + destinations.length)
    //     if (index > -1) {
    //         destinations.splice(index, 1);
    //         console.log("new length of array: " + destinations.length)
    //         Alert.alert("Done","Deleted");
    //
    //         this._persistData();
    //     }
    //     else{
    //         Alert.alert("Warning","Request not found. Can't delete");
    //     }
    //     console.log("------------------------ DELETE done  -------------");
    //
    //     this.props.navigator.push({id :1});
    // }

    _handlePressDelete(){
        var query = this.itemsRef.orderByChild('name').equalTo(this.props.destination.name);
        query.on('child_added', function(snapshot) {
            snapshot.ref.remove();
        })


        this.props.navigator.push({id :4,
            passProps : {
                role : this.state.role
            }
        });
    }

    _renderEditButton(){
        if(this.state.role == 0){
            return(<Button
                containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'green'}}
                style={{fontSize: 18, color: '#F88017'}}
                styleDisabled={{color: 'grey'}}
                onPress={() => this._handlePress()}>
                Save
            </Button>)
        } else return null;
    }

    _renderDeleteButton(){
        console.log("from delete" + this.state.role)
        if(this.state.role == 0){
            return(<Button
                containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: '#FFFF00'}}
                style={{fontSize: 20, color: 'red'}}
                styleDisabled={{color: 'red'}}
                onPress={() => this._handlePressDelete()}>
                Delete
            </Button>)
        }
        else return null;
    }

    render() {
        return (
            <ScrollView style={{padding: 10}}>
              <TextInput
                  style={{height: 60}}
                  ref= "name"
                  onChangeText={(name) => this.setState({name})}
                  value={this.state.name}
              />
              <TextInput
                  style={{height: 60}}
                  keyboardType='number-pad'
                  ref= "rating"
                  onChangeText={(rating) => this.setState({rating})}
                  value={this.state.rating}
              />
              <TextInput
                  style={{height: 60}}
                  ref= "review"
                  onChangeText={(review) => this.setState({review})}
                  value={this.state.review}
              />
                <TextInput
                    style={{height: 60}}
                    ref= "coordinates"
                    onChangeText={(coordinates) => this.setState({coordinates})}
                    value={this.state.coordinates}
                />
                {this._renderEditButton()}
              <Text>
              </Text>
                {this._renderDeleteButton()}
              <BarChart
                  dataSets={[
						{
						  fillColor: '#46b3f7',
						  data: [
							{ value: this.props.destination.rating1 },
							{ value: this.props.destination.rating2 },
							{ value: this.props.destination.rating3 },
							{ value: this.props.destination.rating4 },
							{ value: this.props.destination.rating5 },
						  ]
						},
					  ]}
                  graduation={1}
                  horizontal={false}
                  showGrid={true}
                  barSpacing={5}
                  style={{
						height: 200,
						margin: 15,
				}}/>
              <Text>
                Chart legend:	First column = Rating 1* {"\n"}
                Second Column = Rating 2* {"\n"}
                Third Column = Rating 3* {"\n"}
                Fourth column = Rating 4* {"\n"}
                Fourth column = Rating 5* {"\n"}
                  {"\n"}
                  {"\n"}
                  {"\n"}
              </Text>

            </ScrollView>
        )
    }
}



class ReactDestination extends Component {
    constructor(props){
        super(props);

        this.itemsRef = Firebase.database().ref();

        const ds = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        });

        if(destinations.length == 0){
            // this._getPersistedData();
            this.listenForItems(this.itemsRef);
        }

        this.state={
            dataSource : ds.cloneWithRows(destinations),
            loaded: true,
            role : this.props.items
        }
    }

    // _getPersistedData(){
    //     return AsyncStorage.getItem('destinationsList')
    //         .then(g => JSON.parse(g))
    //         .then(json => {
    //
    //             for (var i = 0; i < json.length; i++) {
    //                 destinations.push({ "name": json[i].name, "rating": json[i].rating, "review" : json[i].review, "coordinates" : json[i].coordinates,
    //                     "rating1" : json[i].rating1, "rating2" : json[i].rating2 , "rating3" : json[i].rating3, "rating4" : json[i].rating4, "rating5" : json[i].rating5});
    //
    //                 this.setState({
    //                     dataSource: this.state.dataSource.cloneWithRows(destinations),
    //                     loaded: true,
    //                 });
    //             }
    //         })
    //         .catch(error => console.log('error! la cititre !!!!!'));
    //
    // }

    listenForItems(itemsRef) {
        itemsRef.on('value', (snap) => {

            // get children as an array

            while(destinations.length > 0) {
                destinations.pop();
            }

            snap.forEach((child) => {
                destinations.push({ "id": child.val().id, "name": child.val().name, "rating" : child.val().rating, "review" : child.val().review,
                    "rating0" : child.val().rating0, "rating1" : child.val().rating1, "rating2" : child.val().rating2, "rating3" : child.val().rating3, "rating4" : child.val().rating4});
            });

            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(destinations),
                loaded: true,
            });
        });
    }

    _onPressButton(destination) {

        this.props.navigator.push({id: 3,
            passProps:{
                destination : destination,
                role : this.state.role,
                k : destination._key,
            }
        });

    }

    componentDidMont(){

        // for (var i = 0; i < destinations.length; i++) {
        //     destinations.splice(i, 1);
        // }
        // this._getPersistedData();

        this.setState({
            dataSource: ds.cloneWithRows(destinations),
            loaded: true,
        })

    }

    renderDestination(destination){

        return (
            <TouchableOpacity onPress={this._onPressButton.bind(this,destination)}>
              <View
                  style={styles.viewDetails}>
                <Text>{destination.name}</Text>
                <Text>{destination.rating}</Text>
                <Text>{destination.review}</Text>
                <Text>{destination.coordinates}</Text>
              </View>
            </TouchableOpacity>
        );
    }

    render(){
        if (!this.state.loaded){
            return (<Text> Please wait!! </Text>);
        }

        return(
            <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderDestination.bind(this)}
                style={styles.listView}
            />
        );
    }
}


class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            role : props.role
        };
    }

    _renderAddButton(){
            return(
                <Button
                    containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'yellow'}}
                    style={{fontSize: 20, color: 'green'}}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this._handlePress()}>
                    Add new destination!
                </Button>
            )
    }

    _handlePress() {
        console.log(this.state.role);
        this.props.navigator.push({id: 2,
        passProps : {
            role : this.state.role
        }});
    }

    render() {
        return (

            <View style={styles.container}>
              <Text style={styles.welcome}>
                Welcome to MemoTravel!
              </Text>

                {this._renderAddButton()}
              <ReactDestination navigator={this.props.navigator} items={this.state.role} />

            </View>
        );
    }
}

class LoginOrRegister extends Component {
    constructor(props){
        super(props);
    }

    _handleLoginPress(){

        this.props.navigator.push({id: 5,});
    }

    _handleRegisterPress(){
        this.props.navigator.push({id: 6,});
    }

    render (){
        return(
            <View>

                <Text style={styles.welcome}>
                    Welcome to MemoTravel!
                </Text>

                <Button
                    containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'blue'}}
                    style={{fontSize: 20, color: 'green'}}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this._handleLoginPress()}>
                    Login!
                </Button>
                <Text>
                </Text>
                <Button
                    containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'red'}}
                    style={{fontSize: 20, color: 'green'}}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this._handleRegisterPress()}>
                    Register!
                </Button>

            </View>
        )
    }

}

class FirebaseSignUp extends Component{
    constructor(props){
        super(props);


        this.state = {
            loaded: true,
            email: '',
            password: ''
        };
    }

    _signup(){

        this.setState({
            loaded: false
        });


        firebase.auth().createUserWithEmailAndPassword(this.state.email,this.state.password)
            .then((user) => {
                this._goToLogin();
            }).catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;

            if (errorCode === 'auth/wrong-password') {
                alert('Wrong password.');
            } else {
                alert(errorMessage);
            }
            console.log(error);
        });
    }
    _goToLogin(){
        this.props.navigator.push({id: 5,});
    }

    render() {
        return (
            <View>
                <TextInput
                    style={styles.textinput}
                    onChangeText={(text) => this.setState({email: text})}
                    value={this.state.email}
                    placeholder={"Email Address"}
                />
                <TextInput
                    style={styles.textinput}
                    onChangeText={(text) => this.setState({password: text})}
                    value={this.state.password}
                    secureTextEntry={true}
                    placeholder={"Password"}
                />

                <Button
                    containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'white'}}
                    style={{fontSize: 20, color: 'green'}}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this._signup()}>
                    Register
                </Button>
                <Text>
                </Text>
                <Button
                    containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'white'}}
                    style={{fontSize: 20, color: 'green'}}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this._goToLogin()}>
                    Do you want to sign in?
                </Button>

            </View>
        );
    }
}


class FirebaseLogin extends Component{
    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: '',
            loaded: true
        }
    }


    _login(){

        this.setState({
            loaded: false
        });


        firebase.auth().signInWithEmailAndPassword(this.state.email,this.state.password)
            .then((user) => {
                if(this.state.email.includes("admin")){
                    this.props.navigator.push({id: 4,
                        passProps:{
                            role : 0,
                        }});
                    console.log("admin role" + this.state.role)
                }else{
                    console.log("user role" + this.state.role)
                    this.props.navigator.push({id:4,
                        passProps:{
                            role : 1,
                        }});
                }


                console.log("Logged with " + this.state.email);
            }).catch(function(error){
            var errorCode = error.code;
            var errorMessage = error.message;

            if (errorCode === 'auth/wrong-password') {
                alert('Wrong password.');
            } else {
                alert(errorMessage);
            }
            console.log(error);
        });
    }
    _goToSignup(){
        this.props.navigator.push({id: 6,});
    }

    render(){
        return (
            <View>

                <TextInput
                    style={styles.textinput}
                    onChangeText={(text) => this.setState({email: text})}
                    value={this.state.email}
                    placeholder={"Email Address"}
                />
                <TextInput
                    style={styles.textinput}
                    onChangeText={(text) => this.setState({password: text})}
                    value={this.state.password}
                    secureTextEntry={true}
                    placeholder={"Password"}
                />

                <Button
                    containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'white'}}
                    style={{fontSize: 20, color: 'green'}}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this._login()}>
                    Login!
                </Button>
                <Text>
                </Text>
                <Button
                    containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'white'}}
                    style={{fontSize: 20, color: 'green'}}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this._goToSignup()}>
                    New Here?
                </Button>
            </View>
        );
    }
}

var MemoTravelRN = React.createClass({
    _renderScene(route, navigator) {
        if (route.id === 1) {
            return <LoginOrRegister navigator={navigator} {...route.passProps}/>
        } else if (route.id === 2) {
            return <MyScene navigator={navigator} {...route.passProps}/>
        } else if (route.id === 3){
            return <EditScene navigator={navigator} {...route.passProps} />
        }else if (route.id === 4){
            return <App navigator={navigator} {...route.passProps}/>
        }else if (route.id === 5){
            return <FirebaseLogin navigator={navigator} {...route.passProps} />
        }else if (route.id === 6){
            return <FirebaseSignUp navigator={navigator} {...route.passProps} />
        }
    },
    _configureScene(route) {
        return CustomSceneConfig;
    },

    render() {
        return (
            <Navigator
                initialRoute={{id: 1, }}
                renderScene={this._renderScene}
                configureScene={this._configureScene} />
        );
    }
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F7E7CE',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    listView: {
        width: 300,
        paddingTop: 20,
        backgroundColor: '#F7E7CE',
    },
    instructions: {
        textAlign: 'center',
        color: 'darkred',
        marginBottom: 5,
    },
    viewDetails: {
        margin: 5
    },
});

AppRegistry.registerComponent('MemoTravelRN', () => MemoTravelRN);
