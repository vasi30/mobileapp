MemoTravel
===========

MemoTravel will give you the best recommandations regarding places to visit in your small trips. 

The application will allow users to create an account. After creating the account, a welcome screen appears together with a list of recommended destinations.
At any given moment, the user can use the search bar to search for a location. The application will return a list with tips and points of interest in that zone.

The menu contains the following options:
- insert a new location in which the user will be able to share his location and sync it with the global db.
- statistics => a chart regarding the number of points of interest in each zone
- what`s nearby? => phone coordinates will be displayed on google maps, together with the recommandations nearby

Also, there is a special role called Administrator that can edit other users and edit any submitted location.

If user is not online, his location will be saved locally and submitted online at the next connection.