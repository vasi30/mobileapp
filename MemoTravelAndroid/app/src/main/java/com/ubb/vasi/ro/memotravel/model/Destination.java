package com.ubb.vasi.ro.memotravel.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Vasi on 02.11.2016.
 */

public class Destination implements Serializable{
    private String id;
    private String name;
    private String coordinates;
    private Integer rating;
    private String review;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private ArrayList<Integer> ratingList = new ArrayList<>();

    public Destination(Destination d) {
        this.name = d.getName();
        this.coordinates = d.getCoordinates();
        this.rating = d.getRating();
        this.review = d.getReview();
        ratingList.add(0);
        ratingList.add(0);
        ratingList.add(0);
        ratingList.add(0);
        ratingList.add(0);
    }

    public Destination(String id, String name, String coordinates, Integer rating, String review, ArrayList<Integer> ratings) {
        this.id = id;
        this.name = name;
        this.coordinates = coordinates;
        this.rating = rating;
        this.review = review;
        this.ratingList = ratings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Destination that = (Destination) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (coordinates != null ? !coordinates.equals(that.coordinates) : that.coordinates != null)
            return false;
        if (rating != null ? !rating.equals(that.rating) : that.rating != null) return false;
        return review != null ? review.equals(that.review) : that.review == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (coordinates != null ? coordinates.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (review != null ? review.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }

    public ArrayList<Integer> getRatingList() {
        return ratingList;
    }

    public void setRatingList(ArrayList<Integer> ratingList) {
        this.ratingList = ratingList;
    }
}
