package com.ubb.vasi.ro.memotravel.repository;

import com.ubb.vasi.ro.memotravel.model.Destination;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vasi3 on 05.12.2016.
 */

public class MockUpRepository implements IRepository, Serializable{
    private ArrayList<Destination> itemList;

    public MockUpRepository() {
        itemList = getMockData();
    }

    @Override

    public ArrayList<Destination> getAllDestinations() {
        return getMockData();
    }

    @Override
    public void addDestination(Destination destination) {
        itemList.add(destination);
    }

    @Override
    public void updateDestination(String id, Destination destination) {
        Destination d;
        for (Destination dest:itemList) {
            if(dest.getId().equals(id)){
                d = dest;
                break;
            }
        }
        d = new Destination(destination);
    }

    @Override
    public void deleteDestination(String id) {
        for (Destination dest:itemList) {
            if(dest.getId().equals(id)){
                itemList.remove(dest);
                break;
            }
        }
    }


    private ArrayList<Destination> getMockData(){
        ArrayList<Destination> itemList = new ArrayList<>();
//
//        itemList.add(new Destination("Tahiti","0,0", 5, "It`s a magical place"));
//        itemList.add(new Destination("Cluj Napoca","23,120", 3, "Mhmm"));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));
//        itemList.add(new Destination("Bucharest","212,320", 1, "S..."));

        return itemList;
    }
}
