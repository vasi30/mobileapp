package com.ubb.vasi.ro.memotravel.repository;

import com.ubb.vasi.ro.memotravel.model.Destination;

import java.util.ArrayList;

/**
 * Created by vasi3 on 06.12.2016.
 */

public interface IRepository {
    ArrayList<Destination> getAllDestinations();
    void addDestination(Destination destination);
    void updateDestination(String id, Destination destination);
    void deleteDestination(String id);
}
