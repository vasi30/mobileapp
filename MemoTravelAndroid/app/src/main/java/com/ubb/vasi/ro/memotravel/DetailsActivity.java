package com.ubb.vasi.ro.memotravel;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ubb.vasi.ro.memotravel.controller.DestinationsController;
import com.ubb.vasi.ro.memotravel.model.Destination;
import com.ubb.vasi.ro.memotravel.repository.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    TextView nameBox;
    TextView coordinatesBox;
    TextView reviewBox;
    TextView ratingBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        nameBox = (TextView)findViewById(R.id.itemName);
        coordinatesBox = (TextView)findViewById(R.id.itemCoordinates);
        reviewBox = (TextView)findViewById(R.id.itemReview);
        ratingBox = (TextView)findViewById(R.id.itemRating);

        final Destination dest = (Destination) getIntent().getSerializableExtra("Destination");
        if (dest != null) {
            nameBox.setText(dest.getName());
            coordinatesBox.setText(dest.getCoordinates());
            reviewBox.setText(dest.getReview());
            ratingBox.setText(dest.getRating()+"");
            //The key argument here must match that used in the other activity
        }

        FloatingActionButton editFab = (FloatingActionButton) findViewById(R.id.fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailsActivity.this, EditDestinationActivity.class);
                intent.putExtra("Destination", dest);
                startActivity(intent);
            }
        });

        FloatingActionButton deletefab = (FloatingActionButton) findViewById(R.id.deletefab);
            deletefab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(DetailsActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Closing Activity")
                            .setMessage("Are you sure you want to delete this destination?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Repository.deleteDestination(dest.getId());
//                                try {
//                                    createFile();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
                                    finish();
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            });

        FloatingActionButton chartButton = (FloatingActionButton) findViewById(R.id.chartBtn);
        chartButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), ChartActivity.class);
                intent.putExtra("ratingValues", dest.getRatingList());
                startActivity(intent);

                Toast toast = Toast.makeText(DetailsActivity.this,"Destination ratings chart ", Toast.LENGTH_SHORT);
                toast.show();

//                Intent intent = new Intent(GameDetailActivity.this, ChartSecondActivity.class);
//                startActivity(intent);
            }
        });
        if(Repository.getCurrentUser().getRole() != 0) {
            editFab.setVisibility(View.GONE);
            deletefab.setVisibility(View.GONE);
        }
    }

    public void createFile() throws IOException, JSONException {
        JSONArray data = new JSONArray();
        JSONObject destination;

        for(int i = 0; i< Repository.getAllDestinations().size(); i++){
            destination = new JSONObject();
            destination.put("id",Repository.getAllDestinations().get(i).getId());
            destination.put("name",Repository.getAllDestinations().get(i).getName());
            destination.put("rating",Repository.getAllDestinations().get(i).getRating());
            destination.put("review",Repository.getAllDestinations().get(i).getReview());
            destination.put("coordinates",Repository.getAllDestinations().get(i).getCoordinates());
            destination.put("rating1",Repository.getAllDestinations().get(i).getRatingList().get(0));
            destination.put("rating2",Repository.getAllDestinations().get(i).getRatingList().get(1));
            destination.put("rating3",Repository.getAllDestinations().get(i).getRatingList().get(2));
            destination.put("rating4",Repository.getAllDestinations().get(i).getRatingList().get(3));
            destination.put("rating5",Repository.getAllDestinations().get(i).getRatingList().get(4));
            data.put(destination);
        }

        String text = data.toString();

        FileOutputStream fos = openFileOutput("destinationsFile", MODE_PRIVATE);
        fos.write(text.getBytes());
        fos.close();

        Toast toast = Toast.makeText(DetailsActivity.this,"Data saved", Toast.LENGTH_SHORT);
        toast.show();
    }
}
