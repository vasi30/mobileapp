package com.ubb.vasi.ro.memotravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.keepSynced(true);

    }

    public void btnRegistration_Click(View v){

        Intent reg = new Intent(MainActivity.this, RegistrationActivity.class);
        startActivity(reg);
    }

    public void btnLogin_Click(View v){
        Intent login = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(login);
    }
}
