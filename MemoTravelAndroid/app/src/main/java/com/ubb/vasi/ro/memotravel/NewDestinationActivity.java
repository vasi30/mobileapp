package com.ubb.vasi.ro.memotravel;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ubb.vasi.ro.memotravel.controller.DestinationsController;
import com.ubb.vasi.ro.memotravel.model.Destination;
import com.ubb.vasi.ro.memotravel.repository.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class NewDestinationActivity extends AppCompatActivity {

    private Intent emailIntent;
    private EditText idBox;
    private EditText nameBox;
    private EditText coordinatesBox;
    private EditText ratingBox;
    private EditText reviewBox;
    private EditText emailBox;
    private EditText rating1Box;
    private EditText rating2Box;
    private EditText rating3Box;
    private EditText rating4Box;
    private EditText rating5Box;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_destination);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        idBox = (EditText) findViewById(R.id.idBox);
        nameBox = (EditText) findViewById(R.id.nameBox);
        coordinatesBox = (EditText) findViewById(R.id.coordinatesBox);
        ratingBox = (EditText) findViewById(R.id.ratingBox);
        reviewBox = (EditText) findViewById(R.id.reviewBox);
        emailBox = (EditText) findViewById(R.id.emailBox);
        rating1Box = (EditText) findViewById(R.id.rating1Box);
        rating2Box = (EditText) findViewById(R.id.rating2Box);
        rating3Box = (EditText) findViewById(R.id.rating3Box);
        rating4Box = (EditText) findViewById(R.id.rating4Box);
        rating5Box = (EditText) findViewById(R.id.rating5Box);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               sendMessage(view);
            }
        });
    }
    /**
     * Called when the user clicks the Send button
     */
    public void sendMessage(View view) {
        // Acquire feedback from an EditText and save it to a String.
        String id = idBox.getText().toString();
        String name = nameBox.getText().toString();
        String coordinates = coordinatesBox.getText().toString();
        Integer rating = Integer.parseInt(ratingBox.getText().toString());
        String review = reviewBox.getText().toString();
        String email = emailBox.getText().toString();

        ArrayList<Integer> ratings = new ArrayList<>();

        ratings.add(Integer.parseInt(rating1Box.getText().toString()));
        ratings.add(Integer.parseInt(rating2Box.getText().toString()));
        ratings.add(Integer.parseInt(rating3Box.getText().toString()));
        ratings.add(Integer.parseInt(rating4Box.getText().toString()));
        ratings.add(Integer.parseInt(rating5Box.getText().toString()));


        Destination d = new Destination(id, name, coordinates, rating, review, ratings);
        Repository.addDestination(d);
        // Create the Intent, and give it the pre-defined value
// that the Android machine automatically associates with
// sending an email.
        emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");

// Put extra information into the Intent, including the email address
// that you wish to send to, and any subject (optional, of course).
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"vasi30@gmail.com"});
        //emailIntent.setData(Uri.parse("mailto:vasi30@gmail.com"));
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "New location data");


// Put the message into the Intent as more extra information,
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Name: "+name+"\n"+
        "Coordinates: " + coordinates +"\n" +
        "Rating: " + rating + "\n" +
        "Review: " + review +
        "Email: " + email);

// Start the Intent, which will launch the user's email

//        try {
//            createFile();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        Toast toast = Toast.makeText(NewDestinationActivity.this,"Destination added to list. confirmation e-mail sent", Toast.LENGTH_SHORT);
        toast.show();

        startActivity(Intent.createChooser(emailIntent, "Send email..."));
        Intent addNew = new Intent(NewDestinationActivity.this, WelcomeActivity.class);
        startActivity(addNew);
    }

    public void createFile() throws IOException, JSONException {
        JSONArray data = new JSONArray();
        JSONObject destination;

        for(int i = 0; i< Repository.getAllDestinations().size(); i++){
            destination = new JSONObject();
            destination.put("id",Repository.getAllDestinations().get(i).getId());
            destination.put("name",Repository.getAllDestinations().get(i).getName());
            destination.put("rating",Repository.getAllDestinations().get(i).getRating());
            destination.put("review",Repository.getAllDestinations().get(i).getReview());
            destination.put("coordinates",Repository.getAllDestinations().get(i).getCoordinates());
            destination.put("rating1",Repository.getAllDestinations().get(i).getRatingList().get(0));
            destination.put("rating2",Repository.getAllDestinations().get(i).getRatingList().get(1));
            destination.put("rating3",Repository.getAllDestinations().get(i).getRatingList().get(2));
            destination.put("rating4",Repository.getAllDestinations().get(i).getRatingList().get(3));
            destination.put("rating5",Repository.getAllDestinations().get(i).getRatingList().get(4));
            data.put(destination);
        }

        String text = data.toString();

        FileOutputStream fos = openFileOutput("destinationsFile", MODE_PRIVATE);
        fos.write(text.getBytes());
        fos.close();

        Toast toast = Toast.makeText(NewDestinationActivity.this,"Data saved", Toast.LENGTH_SHORT);
        toast.show();
    }
}
