package com.ubb.vasi.ro.memotravel;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.RemoteMessage;
import com.ubb.vasi.ro.memotravel.controller.DestinationsController;
import com.ubb.vasi.ro.memotravel.model.Destination;
import com.ubb.vasi.ro.memotravel.repository.Repository;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity {
    private ListView destinationsList;

    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference destinationsRef;

    public WelcomeActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        destinationsRef = databaseReference.child("Destinations");
        destinationsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Destination> itemList = new ArrayList<>();

                for(DataSnapshot destination : dataSnapshot.getChildren()){
                    String id = destination.child("id").getValue().toString();
                    String name = destination.child("name").getValue().toString();
                    String coordinates = destination.child("coordinates").getValue().toString();
                    Integer rating = Integer.parseInt(destination.child("rating").getValue().toString());
                    String review = destination.child("review").getValue().toString();
                    ArrayList<Integer> ratingList = new ArrayList<Integer>();
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("0").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("1").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("2").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("3").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("4").getValue().toString()));

                    Destination d = new Destination(id, name, coordinates, rating, review, ratingList);
                    d.setUserId(Repository.getCurrentUserId());
                    itemList.add(d);
                }
                destinationsList = (ListView) findViewById(R.id.list_view);

                final ArrayAdapter<Destination> adapter = new ArrayAdapter<Destination>(WelcomeActivity.this,android.R.layout.simple_list_item_1, itemList);
                destinationsList.setAdapter(adapter);

                destinationsList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                        final Destination item = (Destination) parent.getItemAtPosition(position);
                        view.animate().setDuration(500).alpha(0.5f)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.notifyDataSetChanged();
                                        view.setAlpha(1f);
                                        Intent appInfo = new Intent(WelcomeActivity.this, DetailsActivity.class);
                                        appInfo.putExtra("Destination", item);

                                        startActivity(appInfo);
                                    }
                                });
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
//
//        try{
//            readFile();
//            System.out.println("Success");
//            Toast toast = Toast.makeText(WelcomeActivity.this,"Success", Toast.LENGTH_SHORT);
//            toast.show();
//
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//            Toast toast = Toast.makeText(WelcomeActivity.this,e.getMessage(), Toast.LENGTH_SHORT);
//            toast.show();
//        }


    }

    public void openAddNew(View view){
        Intent addNew = new Intent(WelcomeActivity.this, NewDestinationActivity.class);
        startActivity(addNew);
    }

    @Override
    public void onRestart() {
        super.onRestart();

        setContentView(R.layout.activity_welcome);

        try{
            //readFile();
            System.out.println("Success");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        destinationsList = (ListView) findViewById(R.id.list_view);


        final ArrayAdapter<Destination> adapter=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, Repository.getAllDestinations());
        destinationsList.setAdapter(adapter);

        destinationsList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                final Destination item = (Destination) parent.getItemAtPosition(position);
                view.animate().setDuration(500).alpha(0.5f)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                                view.setAlpha(1f);
                                Intent appInfo = new Intent(WelcomeActivity.this, DetailsActivity.class);
                                appInfo.putExtra("Destination", item);

                                startActivity(appInfo);
                            }
                        });
            }
        });
    }

    public void readFile() throws IOException, JSONException {
        Repository.clear();
        FileInputStream fis = openFileInput("destinationsFile");
        BufferedInputStream bis = new BufferedInputStream(fis);
        StringBuffer b = new StringBuffer();
        while (bis.available() != 0){
            char c = (char) bis.read();
            b.append(c);
        }
        bis.close();
        fis.close();

        JSONArray data = new JSONArray(b.toString());

        for(int i=0; i<data.length(); i++){
            String id = data.getJSONObject(i).getString("id");
            String name = data.getJSONObject(i).getString("name");
            Integer rating = data.getJSONObject(i).getInt("rating");
            String review = data.getJSONObject(i).getString("review");
            String coordinates = data.getJSONObject(i).getString("coordinates");
            Integer rating1 = data.getJSONObject(i).getInt("rating1");
            Integer rating2 = data.getJSONObject(i).getInt("rating2");
            Integer rating3 = data.getJSONObject(i).getInt("rating3");
            Integer rating4 = data.getJSONObject(i).getInt("rating4");
            Integer rating5 = data.getJSONObject(i).getInt("rating5");

            ArrayList<Integer> ratings = new ArrayList<>();

            ratings.add(rating1);
            ratings.add(rating2);
            ratings.add(rating3);
            ratings.add(rating4);
            ratings.add(rating5);

            Destination d = new Destination(id, name,coordinates,rating,review,ratings);
            //Repository.addDestination(d);
        }

    }
}
