package com.ubb.vasi.ro.memotravel;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ubb.vasi.ro.memotravel.model.User;
import com.ubb.vasi.ro.memotravel.repository.Repository;

public class LoginActivity extends AppCompatActivity {

    public static int userRole = 0;
    private static FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static DatabaseReference databaseReference = database.getReference();

    private EditText txtEmailLogin;
    private EditText txtPasswordLogin;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtEmailLogin = (EditText) findViewById(R.id.txtEmailLogin);
        txtPasswordLogin = (EditText) findViewById(R.id.txtPasswordLogin);
        firebaseAuth = FirebaseAuth.getInstance();

    }

    public void btnLoginUser_Click(View v){

        final ProgressDialog progressDialog = ProgressDialog.show(LoginActivity.this, "Please wait...", "Processing...", true);
        (firebaseAuth.signInWithEmailAndPassword(txtEmailLogin.getText().toString(),txtPasswordLogin.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();

                        if(task.isSuccessful()){
                            String s = txtEmailLogin.getText().toString();
                            Repository.setCurrentUserId((task.getResult().getUser().getUid()));

                            databaseReference.child("Users").child(task.getResult().getUser().getUid()).addListenerForSingleValueEvent(
                                    new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Repository.setCurrentUser(dataSnapshot.getValue(User.class));
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                            Toast.makeText(LoginActivity.this, "Welcome", Toast.LENGTH_LONG).show();
                            if(s.contains("admin")){
                                userRole = 1;
                            }

//                            NotificationCompat.Builder mBuilder =
//                                    new NotificationCompat.Builder(LoginActivity.this)
//                                            .setSmallIcon(R.drawable.notification_icon)
//                                            .setContentTitle("Notification")
//                                            .setContentText("Welcome " + s);


//                            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                            mNotificationManager.notify(001,mBuilder.build());

                            Intent i = new Intent(LoginActivity.this, WelcomeActivity.class);
                            startActivity(i);
                        }else{
                            Log.e("Error",task.getException().toString());
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }
}
