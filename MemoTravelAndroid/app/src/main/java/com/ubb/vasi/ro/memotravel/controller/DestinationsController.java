package com.ubb.vasi.ro.memotravel.controller;

import com.ubb.vasi.ro.memotravel.model.Destination;
import com.ubb.vasi.ro.memotravel.repository.IRepository;
import com.ubb.vasi.ro.memotravel.repository.MockUpRepository;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vasi3 on 06.12.2016.
 */

public class DestinationsController implements Serializable{
    private IRepository itemRepository;

    public DestinationsController() {
        itemRepository = new MockUpRepository();
    }

    public ArrayList<Destination> getAll(){
        return itemRepository.getAllDestinations();
    }

    public void addDestination(Destination destination){
        itemRepository.addDestination(destination);
    }

    public void updateDestination(String id, Destination destination){
        itemRepository.updateDestination(id, destination);
    }

    public void deleteDestination(String id){
        itemRepository.deleteDestination(id);
    }
}
