package com.ubb.vasi.ro.memotravel.repository;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.ubb.vasi.ro.memotravel.model.Destination;
import com.ubb.vasi.ro.memotravel.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vasi3 on 11.12.2016.
 */

public class Repository {
    private static ArrayList<Destination> itemList = new ArrayList<>();
    private static FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static DatabaseReference databaseReference = database.getReference();
    private static DatabaseReference destinationsRef;
    private static String currentUserId;
    private static User currentUser;

    public static void setCurrentUser(User currentUser) {
        Repository.currentUser = currentUser;
    }

    public static String getCurrentUserId() {
        return currentUserId;
    }

    private static HashMap<String, Destination> itemMapList = new HashMap<>();

    public Repository() {
    }

    public static ArrayList<Destination> getAllDestinations() {
        databaseReference.keepSynced(true);

        destinationsRef = databaseReference.child("Destinations");
        destinationsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                itemList = new ArrayList<>();

                for(DataSnapshot destination : dataSnapshot.getChildren()){
                    String id = destination.child("id").getValue().toString();
                    String name = destination.child("name").getValue().toString();
                    String coordinates = destination.child("coordinates").getValue().toString();
                    Integer rating = Integer.parseInt(destination.child("rating").getValue().toString());
                    String review = destination.child("review").getValue().toString();
                    ArrayList<Integer> ratingList = new ArrayList<Integer>();
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("0").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("1").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("2").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("3").getValue().toString()));
                    ratingList.add(Integer.parseInt(destination.child("ratingList").child("4").getValue().toString()));

                    Destination d = new Destination(id, name, coordinates, rating, review, ratingList);
                    d.setUserId(currentUserId);
                    itemList.add(d);
                    itemMapList.put(d.getId(), d);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return itemList;
    }


    public static void addDestination(Destination destination) {
//        itemList.add(destination);
//        itemMapList.put(destination.getId(), destination);
        destination.setUserId(currentUserId);
        DatabaseReference newRef = databaseReference.child("Destinations").push();
        destination.setId(newRef.getKey());
        newRef.setValue(destination);

    }

    public static void updateDestination(String id, Destination destination) {
//        itemMapList.put(id, destination);
//        Destination d;
//        int pos = 0;
//        for (Destination dest:itemList) {
//            if(dest.getId().equals(id)){
//                d = dest;
//                break;
//            }
//            pos++;
//        }
        destinationsRef = databaseReference.child("Destinations").child(id);
        destinationsRef.setValue(destination);
//        itemList.set(pos, destination);
    }

    public static void deleteDestination(String id) {
        destinationsRef = databaseReference.child("Destinations").child(id);
        destinationsRef.setValue(null);
    }

    public static Destination getDestinationById(String id){

        return itemMapList.get(id);
    }

    public static void clear(){
        itemList.clear();
        itemMapList.clear();
    }
    private static ArrayList<Destination> getMockData(){
        ArrayList<Destination> itemList = new ArrayList<>();
        return itemList;
    }

    public static void saveUser(String userId, String userEmail){
        User appUser = new User(userId, userEmail);

        databaseReference.child("Users").child(userId).setValue(appUser);
    }

    public static void setCurrentUserId(String userId){
        currentUserId = userId;
    }

    public static User getCurrentUser(){
        return currentUser;
    }
}
