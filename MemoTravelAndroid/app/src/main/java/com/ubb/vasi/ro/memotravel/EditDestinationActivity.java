package com.ubb.vasi.ro.memotravel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ubb.vasi.ro.memotravel.model.Destination;
import com.ubb.vasi.ro.memotravel.repository.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;

public class EditDestinationActivity extends AppCompatActivity {

    private EditText nameBox;
    private EditText coordinatesBox;
    private EditText ratingBox;
    private EditText reviewBox;
    private EditText emailBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_destination);

        nameBox = (EditText) findViewById(R.id.nameBox);
        coordinatesBox = (EditText) findViewById(R.id.coordinatesBox);
        ratingBox = (EditText) findViewById(R.id.ratingBox);
        reviewBox = (EditText) findViewById(R.id.reviewBox);

        final Destination dest = (Destination) getIntent().getSerializableExtra("Destination");
        if (dest != null) {
            nameBox.setText(dest.getName());
            coordinatesBox.setText(dest.getCoordinates());
            reviewBox.setText(dest.getReview());
            ratingBox.setText(dest.getRating()+"");
            emailBox = (EditText) findViewById(R.id.emailBox);
            //The key argument here must match that used in the other activity
        }

        Button editButton = (Button)findViewById(R.id.edit);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dest.setName(nameBox.getText().toString());
                dest.setRating(Integer.parseInt(ratingBox.getText().toString()));
                dest.setCoordinates(coordinatesBox.getText().toString());
                dest.setReview(reviewBox.getText().toString());
                Repository.updateDestination(dest.getId(), dest);
//                try {
//                    createFile();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                finish();
            }
        });
    }

    public void createFile() throws IOException, JSONException {
        JSONArray data = new JSONArray();
        JSONObject destination;

        for(int i = 0; i< Repository.getAllDestinations().size(); i++){
            destination = new JSONObject();
            destination.put("id",Repository.getAllDestinations().get(i).getId());
            destination.put("name",Repository.getAllDestinations().get(i).getName());
            destination.put("rating",Repository.getAllDestinations().get(i).getRating());
            destination.put("review",Repository.getAllDestinations().get(i).getReview());
            destination.put("coordinates",Repository.getAllDestinations().get(i).getCoordinates());
            destination.put("rating1",Repository.getAllDestinations().get(i).getRatingList().get(0));
            destination.put("rating2",Repository.getAllDestinations().get(i).getRatingList().get(1));
            destination.put("rating3",Repository.getAllDestinations().get(i).getRatingList().get(2));
            destination.put("rating4",Repository.getAllDestinations().get(i).getRatingList().get(3));
            destination.put("rating5",Repository.getAllDestinations().get(i).getRatingList().get(4));
            data.put(destination);
        }

        String text = data.toString();

        FileOutputStream fos = openFileOutput("destinationsFile", MODE_PRIVATE);
        fos.write(text.getBytes());
        fos.close();

        Toast toast = Toast.makeText(EditDestinationActivity.this,"Data saved", Toast.LENGTH_SHORT);
        toast.show();
    }
}
