package com.ubb.vasi.ro.memotravel.model;

import java.io.Serializable;

/**
 * Created by vasi3 on 05.12.2016.
 */

public class User implements Serializable {
    private String id;
    private String email;
    private int role;

    public User(String id, String email, Integer role) {
        this.id = id;
        this.email = email;
        this.role = role;
    }

    public User(String id, String email) {
        this.id = id;
        this.email = email;
        this.role = 0;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
